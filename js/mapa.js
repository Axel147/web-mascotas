'use strict'

var ungsLocation = [-34.5221554, -58.7000067];
class Mapa {
    constructor(idMapa){

        this.mapa = L.map(idMapa).setView(ungsLocation, 13);
        this.layerMarkers = new L.LayerGroup;

        this.layerMarkers.addTo(this.mapa);
        L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
        attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
        }).addTo(this.mapa);

    }

    dibujarMarker(coordenadas, nombre, direccion, telefono){
        var marker = new L.marker(coordenadas);
        marker.addTo(this.layerMarkers).bindPopup("<b>" + nombre + "<br>" + direccion + "<br>" + telefono);
        return marker;
    }

    quitarMarker(marker){
        this.mapa.removeLayer(marker);
    }

    limpiarMarkers(){
        this.layerMarkers.clearLayers();
    }

    centrarVista(posicion){
        this.mapa.setView(posicion, 17);
    }

    contarMarkers(){
        return Object.values(this.layerMarkers._layers).length;
    }

    obtenerUnicoMarker(){
        return Object.values(this.layerMarkers._layers)[0];
    }

    quitarZoom(){
        this.mapa.setView(ungsLocation, 13);
    }
}

