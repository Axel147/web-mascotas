$( document ).ready( function(){

    $('#filtradorAvisos input[type="radio"]').on('change', function(){
        let seleccionado = $(this).attr('value');
        $('#listadoAvisos .aviso').each( (i, aviso) => {
            if( seleccionado == 'Todos' )
                $(aviso).show();
            else if( $(aviso).attr('type') != seleccionado )
                $(aviso).hide();
            else
                $(aviso).show();
        });
    })

})

$( document ).ready( function(){

    var listadoAvisos = $('#listadoAvisos');
    
    avisos.forEach( aviso => {
        template = prepararTemplateAviso(aviso);
        console.log(template);
        $(listadoAvisos).append(template);          
    })
    
    $('.carousel').carousel({
        interval: 2000
    })


});

function prepararTemplateAviso({idAviso, tipoAviso, fechaSuceso, perfilMascota, datosContacto}){

    let coloresBadge = {
        'Mascota en adopcion': 'badge-info',
        'Mascota encontrada': 'badge-success',
        'Mascota perdida': 'badge-danger'
    }

    let contenidoBadge = (tipoAviso == 'Mascota en adopcion')? `${tipoAviso}`: `${tipoAviso}: ${fechaSuceso}`
    let colorBadge = coloresBadge[tipoAviso];

    let fotos = "";

    perfilMascota.foto.forEach((foto,i)=>{
    fotos = fotos.concat(`<div class="carousel-item ${(i == 0)? 'active':''}">
        <img class="d-block w-100" src="${foto}" alt="First slide">
        </div>`)
    })


    let template = `<div class="card aviso" type="${tipoAviso}">
                            <div class="row">
                                <div class="col-md-4 col-sm-12 my-auto">
                                <div id="carousel${idAviso}" class="carousel slide" data-ride="carousel">
                                    <div class="carousel-inner">
                                        ${fotos}
                                    </div>
                                    <a class="carousel-control-prev" href="#carousel${idAviso}" role="button" data-slide="prev">
                                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                        <span class="sr-only">Previous</span>
                                    </a>
                                    <a class="carousel-control-next" href="#carousel${idAviso}" role="button" data-slide="next">
                                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                        <span class="sr-only">Next</span>
                                    </a>
                                    </div>
                                </div>
                                <div class="col-md-8 col-sm-12">
                                    <div class="card-block px-2">
                                        <h4 class="card-title">
                                            <span class="badge ${colorBadge}">${contenidoBadge}</span>
                                        </h4>
                                        <br>
                                        <div class="row">
                                            <div class="col">
                                                <h5 style="font-weight: bold;">Perfil de la mascota</h5>
                                                <div class="form-group">
                                                    <label class="control-label">Nombre</label>
                                                    <p class="form-control-static boxed">${perfilMascota.nombre}</p>
                                                    <label class="control-label">Raza</label>
                                                    <p class="form-control-static boxed">${perfilMascota.raza}</p>
                                                    <label class="control-label">Edad Aproximada</label>
                                                    <p class="form-control-static boxed">${perfilMascota.edadAproximada}</p>
                                                    <label class="control-label">Descripcion</label>
                                                    <p class="form-control-static boxed">${perfilMascota.descripcion}</p>
                                                </div>
                                            </div>
                                            <div class="col">
                                                <h5 style="font-weight: bold;">Datos de contacto</h5>
                                                <div class="form-group">
                                                    <label class="control-label">Direccion</label>
                                                    <p class="form-control-static boxed">${datosContacto.direccion}</p>
                                                    <label class="control-label">E-Mail</label>
                                                    <p class="form-control-static boxed">${datosContacto.correo}</p>
                                                    <label class="control-label">Telefono</label>
                                                    <p class="form-control-static boxed">${datosContacto.telefono}</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>`;
    return template;

}