var comercios = [
    {
	idComercio: '1',
	nombreDueño: 'Carlitos Perez',
	servicio:'Peluqueria canina',
	nombreLocal: 'Carlinos',
	descripcion: 'Ofertas todos los fines de semana. Se hacen traslados.',
    foto: './img/Carlinos.jpg',
    esPatrocinador: true,
	datosContacto: {
		direccion: 'Carlos Pellegrini 1064',
		horario: '10-18 Hs.',
        correo: 'soyelcarlitos@gmail.com',
		telefono: '4465-0000'
	},
	coordenadas:{lat:-34.520760, lon: -58.702384}
   },
   {
	idComercio: '2',
	nombreDueño: 'Marta Gonzales',
	servicio:'Comida de mascotas',
	nombreLocal: 'Alimenta a tus pobres Marcotas',
	descripcion: 'Tenemos Sabrocitos como en la propaganda para tus perros y whiskas para tu gato porque recorda que 8 de cada 10 gatos prefieren whiskas',
    foto: 'http://www.mitiendademascotas.com/web/comodororivadavia/wp-content/uploads/sites/19/2017/07/WalMart-Centro-CDR-1.jpg',
    esPatrocinador: false,
	datosContacto: {
		direccion: 'Baker Street 221B',
		horario: '9-13 / 16-20 Hs.',
        correo: 'gonzalesmt23@outlook.com',
		telefono: '4465-0001'
	},
	coordenadas:{lat: -34.522981, lon:-58.706181}
   },
   {
	idComercio: '3',
	nombreDueño: 'Cesar Millan',
	servicio:'Encantador de perros',
	nombreLocal: 'El lider de la manada',
	descripcion: 'Adiestrador de perros y venta de articulos de mascotas en general',
    foto: 'http://www.mitiendademascotas.com/web/comodororivadavia/wp-content/uploads/sites/19/2017/07/Walmart-Centro-CDR.jpg',
    esPatrocinador: true,
	datosContacto: {
		direccion: 'Gotham y Metropolis',
		horario: '9-13:30 / 17-20:30 Hs.',
        correo: 'nosoybatman@gmail.com',
		telefono: '555-0123'
	},
	coordenadas:{lat: -34.520544, lon: -58.705999}
   },
   {
	idComercio: '4',
	nombreDueño: 'Tito el Bambino',
	servicio:'Clinica veterinaria',
	nombreLocal: 'Hospipet Rivas',
	descripcion: 'Atendemos en nuestro negocio y se hacen visitas a domicilio',
    foto: 'https://www.portalrivas.com/images/hospivet-rivas/hospivet-rivas-1.jpg',
    esPatrocinador: true,
	datosContacto: {
		direccion: 'Pandora y Narnia',
		horario: '8-13 / 16-20 Hs.',
        correo: 'clinicahospipet@gmail.com',
		telefono: '4465-1590'
	},
	coordenadas:{lat: -34.519643, lon: -58.700886}
   }
]