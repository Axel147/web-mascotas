var avisos = [
    {
        perfilMascota: {
               nombre: 'Luchito',
               raza: 'Pastor Aleman',
               descripcion: 'Perro adulto super fachero ',
               edadAproximada: '1 año',
               foto: ['https://www.hola.com/imagenes/estar-bien/20180823128631/cosas-que-quizas-no-sabias-de-tu-pastor-aleman-cs/0-593-146/cosassobrepastoraleman-t.jpg']
        },
        idAviso: '1',
        tipoAviso: 'Mascota perdida',
        fechaSuceso: '20-05-2020',
        datosContacto: {
            direccion: 'calle falsa 123',
            telefono: '1150500911',
            correo: 'buscoaluchito@gmail.com'

        }
    },

    {
        perfilMascota: {
            nombre: 'Beto Ven',
            raza: 'San Bernardo',
            descripcion: 'Perro ideal para pasar el invierno, vino con botellita en el cuello ',
            edadAproximada: '2 años',
            foto: ['https://i.pinimg.com/originals/f3/65/df/f365df1ec06da3ca3fe6edc26dbf44e3.jpg'],
        },
        idAviso: '2',
        tipoAviso: 'Mascota encontrada',
        fechaSuceso: '21-05-2020',
        datosContacto: {
            direccion: 'avenida siempreViva',
            telefono: '1168709095',
            correo: 'perrocasimusico@gmail.com'
        }
    },

    {
        perfilMascota: {
            nombre: 'Firulais yuñor',
            raza: 'Sin raza',
            descripcion: 'Cachorrito blanco y negro simpaticon',
            edadAproximada: '2 meses',
            foto: ['https://i.ytimg.com/vi/X4_-kWf-3x4/maxresdefault.jpg'],
        },
        idAviso: '3',
        tipoAviso: 'Mascota en adopcion',
        fechaSuceso: '19-05-2020',
        datosContacto: {
            direccion: 'mi casa',
            telefono: '1504060010',
            correo: 'noseescribeyoñor@gmail.com'

        }
    },

    {
        perfilMascota: {
            nombre: 'Piki Roberto',
            raza: 'Gerbo',
            descripcion: 'Es negro, y responde al nombre de Piki',
            edadAproximada: '2 meses',
            foto: ['./img/PikiRoberto.jpeg','./img/PikiRoberto2.jpeg','./img/PikiRoberto3.jpeg'],
        },
        idAviso: '4',
        tipoAviso: 'Mascota perdida',
        fechaSuceso: '26-04-2020',
        datosContacto: {
            direccion: 'CABA',
            telefono: '1578945632',
            correo: 'papadepiki@gmail.com'

        }
    },

    {
        perfilMascota: {
            nombre: 'Tomi',
            raza: 'Chihuahua',
            descripcion: 'Es marron, tiene un ojito lastimado',
            edadAproximada: '8 años',
            foto: ['./img/Tomi.jpeg', './img/Tomi2.jpeg'],
        },
        idAviso: '5',
        tipoAviso: 'Mascota en adopcion',
        fechaSuceso: '26-04-2020',
        datosContacto: {
            direccion: 'San Miguel',
            telefono: '1165489654',
            correo: 'unacasaparatomi@gmail.com'

        }
    },

    {
        perfilMascota: {
            nombre: 'Bombon',
            raza: 'Caniche',
            descripcion: 'Negra, pelo rizado',
            edadAproximada: '3 años',
            foto: ['./img/bombon.jpeg', './img/bombon2.jpeg'],
        },
        idAviso: '6',
        tipoAviso: 'Mascota perdida',
        fechaSuceso: '23-02-2020',
        datosContacto: {
            direccion: 'Cerca de la uni',
            telefono: '1145483153',
            correo: 'megustaelchocolate@gmail.com'

        }
    },

]