var apiSMN = 'https://ws.smn.gob.ar';
var apiWeather = 'https://weatherservices.herokuapp.com/api'

$(document).ready( function(){
    mostrarPronostico();
    mostrarAlertasWeather();
    mostrarAlertasSmn();
})

function mostrarPronostico(){

    let urlPronostico = `${apiWeather}/weather`;

    fetch(urlPronostico)
        .then( response => response.json() )
        .then( json => {
            json = json.items[0];
            let template = prepararTemplatePronostico(json);
            $('.weather').append(template);
        });

}

function prepararTemplatePronostico({name, weather, forecast}){
    let template = `<div class="current">
                        <div class="info">
                            <div>&nbsp;</div>
                            <div class="city"><small><small>CIUDAD:</small></small> ${name}, <small>BA</small></div>
                            <div class="temp">${weather.temp}&deg; <small>C</small></div>
                            <div class="city"><small><small>ESTADO:</small></small> ${weather.description}<small></small></div>
                            <div class="wind"><small><small>VIENTO:</small></small> ${weather.wind_speed} km/h</div>
                            <div class="wind"><small><small>HUMEDAD:</small></small> ${weather.humidity} %</div>
                            <div class="wind"><small><small>PRESION:</small></small> ${weather.pressure} hPa</div>
                            <div class="wind"><small><small>VISIBILIDAD:</small></small> ${weather.visibility} km</div>
                            <div>&nbsp;</div>
                        </div>
                    </div>
                    <div class="future">
                        <div class="day" data-toggle="modal" data-target="#modalLun">
                            <h3>Lun</h3>
                            <p>${forecast.forecast[1].temp_max}&deg;<small>/${forecast.forecast[1].temp_min}&deg;</small></p>
                        </div>
                        <div class="modal fade" id="modalLun" tabindex="-1" role="dialog" aria-labelledby="Lunes" aria-hidden="true">
                            <div class="modal-dialog modal-dialog-centered" role="document">
                                <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title">Lunes</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Cerrar">
                                    <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <h5>Mañana</h5>
                                    <p>${forecast.forecast[1].morning.description}</p>
                                    <br>
                                    <h5>Tarde</h5>
                                    <p>${forecast.forecast[1].afternoon.description}</p>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                                </div>
                                </div>
                            </div>
                        </div>
                        <div class="day" data-toggle="modal" data-target="#modalMar">
                            <h3>Mar</h3>
                            <p>${forecast.forecast[2].temp_max}&deg;<small>/${forecast.forecast[2].temp_min}&deg;</small></p>
                        </div>
                        <div class="modal fade" id="modalMar" tabindex="-1" role="dialog" aria-labelledby="Martes" aria-hidden="true">
                            <div class="modal-dialog modal-dialog-centered" role="document">
                                <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title">Martes</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Cerrar">
                                    <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <h5>Mañana</h5>
                                    <p>${forecast.forecast[2].morning.description}</p>
                                    <br>
                                    <h5>Tarde</h5>
                                    <p>${forecast.forecast[2].afternoon.description}</p>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                                </div>
                                </div>
                            </div>
                        </div>
                        <div class="day" data-toggle="modal" data-target="#modalMie">
                            <h3>Mie</h3>
                            <p>${forecast.forecast[3].temp_max}&deg;<small>/${forecast.forecast[3].temp_min}&deg;</small></p>
                        </div>
                        <div class="modal fade" id="modalMie" tabindex="-1" role="dialog" aria-labelledby="Miercoles" aria-hidden="true">
                            <div class="modal-dialog modal-dialog-centered" role="document">
                                <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title">Miercoles</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Cerrar">
                                    <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <h5>Mañana</h5>
                                    <p>${forecast.forecast[3].morning.description}</p>
                                    <br>
                                    <h5>Tarde</h5>
                                    <p>${forecast.forecast[3].afternoon.description}</p>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                                </div>
                                </div>
                            </div>
                        </div>
                    </div>`
    return template;
}

function mostrarAlertasWeather(){
    
    var dia = Math.floor(Math.random() * (4 - 0)) + 0;
    var urlWeather = `${apiWeather}/alerts/byDay/${dia}`
    
    fetch(urlWeather)
        .then(response => response.json())
        .then(json => {
            json.alerts.forEach(alerta => {
                let template = prepararTemplateAlertas(alerta);
                $('#AlertasWeather').append(template);
            });
            
        });

}

function mostrarAlertasSmn(){

    var urlSMN = `${apiSMN}/alerts/type/AL`
    fetch(urlSMN).then(response => response.json()).then(json => {
        json.forEach(alerta => {
            let template = prepararTemplateAlertas(alerta);
            $("#alertasSMN").append(template);
        });    
    })

}

function prepararTemplateAlertas({date, hour, title, status, type, zones, description}){
    var zonas = "";            
    Object.values(zones).forEach(zona => {
        zonas=zonas.concat(`<li>${zona}</li>`);
    })
            
    console.log(date)
    let fecha = moment(date).format('DD/MM/YYYY');
    console.log(fecha)

    let descripcion = "";
    if( type != 'AC' ){
        descripcion = `<div class="row">
            <div class="col">
                <h5 style="font-weight: bold;">Descripcion</h5>
                <div class="form-group">
                    <p class="form-control-static boxed">${description}</p>
                </div>
            </div>
        </div>`
    }

    let template = `<div class="card" id="carta">
                        <div class="row">                
                            <div class="col">
                                <div class="card-block px-2">
                                    <h4 class="card-title">${title}
                                        <span class="badge badge-warning">${ (type=='AC')? '':status } ${fecha} ${hour}</span>
                                    </h4>
                                    <br>
                                    ${descripcion}
                                    <div class="row">
                                        <div class="col">
                                            <h5 style="font-weight: bold;">Zonas</h5>
                                            <div class="form-group">
                                                <ul class="form-control-static boxed">${zonas}</ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>`;
    return template;
}