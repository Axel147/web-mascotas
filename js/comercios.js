var diccionario = {};
var mapa;
function zoomEnMapa(idComercio){
    mapa.centrarVista(diccionario[idComercio].marker.getLatLng());
    diccionario[idComercio].marker.openPopup();
}
$( document ).ready( function(){

    $("#myInput").on("keyup", function() {
        var value = $(this).val().toLowerCase();
        $(".buscarPor").filter(function() {
            $($(this).parent().parent().parent().parent().parent().parent().parent()).toggle($(this).text().toLowerCase().indexOf(value) > -1)
        });

        mapa.limpiarMarkers();

        var allHidden = true;

        $("#listadoComercios .card").each((i, elemento)=>{
            allHidden = allHidden && $(elemento).css('display') == 'none';

            if($(elemento).css('display') == 'none'){
                mapa.quitarMarker(diccionario[$(elemento).attr("idComercio")].marker);
            } else {
                var temp = $(elemento).attr("idComercio")
                diccionario[temp].marker = mapa.dibujarMarker(diccionario[temp].marker.getLatLng(),diccionario[temp].nombreLocal,diccionario[temp].direccionLocal,diccionario[temp].telefonoLocal);
            }
        })
        if(allHidden){
            $("#noResults").show();
        } else {
            $("#noResults").hide();
        }

        if(mapa.contarMarkers()<=1){
            mapa.centrarVista(mapa.obtenerUnicoMarker().getLatLng())
            mapa.obtenerUnicoMarker().openPopup();
        } else {
            mapa.quitarZoom();
        }

    });

    mapa = new Mapa("mapa");

    comercios.forEach( comercio => {
        
        diccionario[comercio.idComercio] = {
            nombreLocal: comercio.nombreLocal,
            direccionLocal: comercio.datosContacto.direccion,
            telefonoLocal: comercio.datosContacto.telefono,
            marker: mapa.dibujarMarker(comercio.coordenadas, comercio.nombreLocal, comercio.datosContacto.direccion, comercio.datosContacto.telefono)
        }
        
        let template = prepararTemplateComercio(comercio);
        $('#listadoComercios').append(template);
    }) 
    
})

function prepararTemplateComercio({idComercio, foto, nombreLocal, servicio, descripcion, datosContacto, esPatrocinador}){

    let template = `<br>
                    <div idComercio="${idComercio}" class="card">
                        <div class="row">
                            <div class="col-md-4 col-sm-12 my-auto">
                                <img src="${foto}" class="img-fluid" alt="">
                            </div>
                            <div class="col-md-8 col-sm-12">
                                <div class="card-block px-2">
                                    <h4 class="card-title">
                                        <span class="badge badge-info">${ esPatrocinador?'patrocinador':'' }</span>
                                    </h4>
                                    <br>
                                    <div class="row">
                                        <div class="col">
                                            <h5 style="font-weight: bold;">Comercio</h5>
                                            <div class="form-group">
                                                <label class="control-label" id="nombre">Nombre</label>
                                                <p class="form-control-static boxed">${nombreLocal}</p>
                                                <label class="control-label">Servicio</label>
                                                <p class="form-control-static boxed buscarPor">${servicio}</p>
                                                <label class="control-label">Descripción</label>
                                                <p class="form-control-static boxed">${descripcion}</p>
                                            </div>
                                            
                                        </div>
                                        <div class="col">
                                            <h5 style="font-weight: bold;">Datos de contacto</h5>
                                            <div class="form-group">
                                                <label class="control-label">Direccion</label>
                                                <p class="form-control-static boxed">${datosContacto.direccion}</p>
                                                <label class="control-label">Horario de atencion</label>
                                                <p class="form-control-static boxed">${datosContacto.horario}</p>
                                                <label class="control-label">Telefono</label>
                                                <p class="form-control-static boxed">${datosContacto.telefono}</p>
                                                <label class="control-label">Correo electronico</label>
                                                <p class="form-control-static boxed">${datosContacto.correo}</p>
                                            </div>
                                            <div class="btn btn-primary" onclick="zoomEnMapa(${idComercio})">Ver en mapa &raquo;</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>`;
    return template;
}